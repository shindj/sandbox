package com.shindj.graphql.entity;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class Post extends BaseEntity {

    @Setter
    private String title;

    @Setter
    private String detail;

    @OneToOne(fetch = FetchType.LAZY)
    private User writer;


}
