package com.shindj.graphql;

import com.shindj.graphql.entity.Post;
import com.shindj.graphql.entity.User;
import com.shindj.graphql.repository.PostRepository;
import com.shindj.graphql.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class DummyDataCreator {

    private final UserRepository userRepository;
    private final PostRepository postRepository;

    @PostConstruct
    @Transactional
    public void createData() {
        User user = new User("홍길동", "용인 디지털벨리");
        userRepository.save(user);

        IntStream.range(0, 10)
                .mapToObj(i -> new Post("타이틀"+i, "내용"+i, user))
                .forEach(postRepository::save);
    }

}
