package com.shindj.graphql.repository;

import com.shindj.graphql.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Long> {
}
