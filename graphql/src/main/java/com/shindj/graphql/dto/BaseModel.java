package com.shindj.graphql.dto;

import com.shindj.graphql.entity.BaseEntity;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class BaseModel {

    @GraphQLQuery(description = "고유 아이디")
    private Long id;

    public BaseModel(BaseEntity baseEntity) {
        this.id = baseEntity.getId();
    }

    public boolean isNew() {
        return this.id == null;
    }

}
