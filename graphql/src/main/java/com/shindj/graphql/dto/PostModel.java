package com.shindj.graphql.dto;

import com.shindj.graphql.entity.Post;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class PostModel extends BaseModel {

    private final String title;

    private final String detail;

    private final UserModel writer;

    public PostModel(Post post) {
        super(post);
        this.title = post.getTitle();
        this.detail = post.getDetail();
        this.writer = new UserModel(post.getWriter());
    }
}
