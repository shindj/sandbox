package com.shindj.graphql.dto;

import com.shindj.graphql.entity.User;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class UserModel extends BaseModel {

    private final String name;

    private final String address;

    public UserModel(User user) {
        super(user);
        this.name = user.getName();
        this.address = user.getAddress();
    }

}
