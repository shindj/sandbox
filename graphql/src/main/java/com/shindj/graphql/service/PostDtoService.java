package com.shindj.graphql.service;

import com.shindj.graphql.dto.PostModel;
import com.shindj.graphql.entity.Post;
import com.shindj.graphql.entity.User;
import com.shindj.graphql.repository.PostRepository;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.annotations.GraphQLSubscription;
import io.leangen.graphql.spqr.spring.annotation.GraphQLApi;
import io.leangen.graphql.spqr.spring.util.ConcurrentMultiRegistry;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@GraphQLApi
@Transactional
@RequiredArgsConstructor
public class PostDtoService {

    private final PostService postService;

    private final UserService userService;

    private final ConcurrentMultiRegistry<Long, FluxSink<PostModel>> subscribers = new ConcurrentMultiRegistry<>();

    @GraphQLMutation
    public void savePost(PostModel postModel) {
        postService.savePost(convertModelToEntity(postModel));
    }

    @GraphQLMutation
    public boolean deletePost(Long id) {
        postService.getPost(id).ifPresent(postService::deletePost);
        return true;
    }

    @GraphQLMutation
    public boolean deletePosts() {
        postService.getPosts().forEach(postService::deletePost);
        return true;
    }

    @GraphQLQuery(description = "게시물")
    public PostModel getPost(Long id) {
        return new PostModel(postService.getPost(id).get());
    }

    @GraphQLQuery
    public List<PostModel> getPosts() {
        return postService.getPosts().stream().map(PostModel::new).collect(Collectors.toList());
    }

    @GraphQLSubscription
    public Publisher<PostModel> postModelChanged(Long id) {
        return Flux.create(subscriber -> subscribers.add(id, subscriber.onDispose(() -> subscribers.remove(id, subscriber))), FluxSink.OverflowStrategy.LATEST);
    }

    private Post convertModelToEntity(PostModel postModel) {
        User user = userService.getUser(postModel.getWriter().getId()).get();

        Post post = postService.getPost(postModel.getId())
                .orElseGet(() -> new Post(postModel.getTitle(), postModel.getDetail(), user));
        post.setDetail(postModel.getDetail());
        post.setTitle(postModel.getTitle());

        return post;
    }
}
