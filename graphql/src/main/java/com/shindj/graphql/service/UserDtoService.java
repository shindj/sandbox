package com.shindj.graphql.service;

import com.shindj.graphql.dto.UserModel;
import com.shindj.graphql.entity.User;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotation.GraphQLApi;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@GraphQLApi
@Transactional
@RequiredArgsConstructor
public class UserDtoService {

    private final UserService userService;

    @GraphQLQuery
    public UserModel getUser(Long id) {
        return new UserModel(userService.getUser(id).orElse(null));
    }

    @GraphQLQuery
    public List<UserModel> getUsers() {
        return userService.getUsers().stream().map(UserModel::new).collect(Collectors.toList());
    }

    @GraphQLMutation
    public void saveUser(@GraphQLArgument(name ="userModel") UserModel userModel) {
        userService.saveUser(convertModelToEntity(userModel));
    }

    @GraphQLMutation
    public void deleteUser(@GraphQLArgument(name ="userModel") UserModel userModel) {
        userService.deleteUser(convertModelToEntity(userModel));
    }

    public User convertModelToEntity(UserModel userModel) {
        User user = userService.getUser(userModel.getId()).orElseGet(User::new);
        user.setName(userModel.getName());
        user.setAddress(userModel.getAddress());
        return user;
    }

}
