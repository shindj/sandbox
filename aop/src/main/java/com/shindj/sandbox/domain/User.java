package com.shindj.sandbox.domain;

import javax.persistence.Entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class User extends BaseEntity {

	private String name;

	public User(String name) {
		this.name = name;
	}

}
