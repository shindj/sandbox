package com.shindj.sandbox.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.shindj.sandbox.domain.User;
import com.shindj.sandbox.repository.UserRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final String CONTEXT_SEPARATOR = "--------------------------";

	private final UserRepository userRepo;

	@Override
	public void createTestUser() {
		User user = userRepo.save(new User("testUser"));
		System.out.println(userRepo.findById(user.getId()).get().getName());
	}

	@Override
	@Transactional
	public void createUserTest__영속성컨텍스트_존재() {
		System.out.println(CONTEXT_SEPARATOR + "영속성컨텍스트O (expected : insert1개, select0개)");
		createTestUser();
	}

	@Override
	public void createUserTest__영속성컨텍스트_미존재() {
		System.out.println(CONTEXT_SEPARATOR + "영속성컨텍스트X (expected : insert1개, select1개)");
		createTestUser();
	}

}
