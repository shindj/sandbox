package com.shindj.sandbox.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProxyUserService implements UserService {

	private final UserServiceImpl userService;

	private final TransactionTemplate trasactionTemplate;

	@Override
	public void createTestUser() {
		System.out.println("--------> [ProxyUserService]");
		userService.createTestUser();
	}

	@Override
	public void createUserTest__영속성컨텍스트_존재() {
		System.out.println("--------> [ProxyUserService]");
		userService.createUserTest__영속성컨텍스트_존재();
	}

	@Override
	public void createUserTest__영속성컨텍스트_미존재() {
		System.out.println("--------> [ProxyUserService]");
		trasactionTemplate.execute(status -> {
			userService.createUserTest__영속성컨텍스트_미존재();
			return null;
		});

	}

}
