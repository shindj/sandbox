package com.shindj.sandbox.service;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.shindj.sandbox.annotation.MyTransactional;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Primary
public class MyTransactionUserService implements UserService {

	private final UserServiceImpl userService;

	@Override
	public void createTestUser() {
		System.out.println("--------> [MyTransactionUserService]");
		userService.createTestUser();
	}

	@Override
	public void createUserTest__영속성컨텍스트_존재() {
		System.out.println("--------> [MyTransactionUserService]");
		userService.createUserTest__영속성컨텍스트_존재();
	}

	@Override
	@MyTransactional
	public void createUserTest__영속성컨텍스트_미존재() {
		System.out.println("--------> [MyTransactionUserService]");
		userService.createUserTest__영속성컨텍스트_미존재();
	}

}
