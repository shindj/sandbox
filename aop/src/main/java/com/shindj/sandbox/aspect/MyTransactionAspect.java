package com.shindj.sandbox.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import lombok.RequiredArgsConstructor;

@Aspect
@Component
@RequiredArgsConstructor
public class MyTransactionAspect {

	private final TransactionTemplate trasactionTemplate;

	@Around("@annotation(com.shindj.sandbox.annotation.MyTransactional)")
	public void withinMyTransaction(ProceedingJoinPoint joinPoint) throws Throwable {
		System.out.println("my transactional.........");
		trasactionTemplate.execute(status -> {

			try {
				return joinPoint.proceed();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		});

	}
}
