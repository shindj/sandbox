package com.shindj.sandbox;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.shindj.sandbox.service.UserService;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class TestRunner implements ApplicationRunner {

	private final UserService userService;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		userService.createUserTest__영속성컨텍스트_미존재();
		userService.createUserTest__영속성컨텍스트_존재();
	}

}
