## AOP
#### : Proxy기반의 SpringAOP를 활용하여, @Transactional의 기본동작을 구현한 @MyTransactional 작성

* UserService : interface
* UserServiceImpl : @Transactional이 걸려있지 않은 구현체
* ProxyUserService : Proxy패턴을 이용하여, UserService에 @Transacational 부여
* MyTransactionService : SpringAOP와 @MyTransacational이라는 CustomAnnotation을 사용하여, UserService에 @Transacational과 같은 동작 부여

----------------------------------------------------------------------------------------------------

### ProxyPattern 정의

일반적으로 프록시는
다른 무언가와 이어지는 인터페이스의 역할을 하는 클래스이다.
프록시는 어떠한 것(이를테면 네트워크 연결, 메모리 안의 커다란 객체, 파일, 또 복제할 수 없거나 수요가 많은 리소스)과도 인터페이스의 역할을 수행할 수 있다. - Wiki 발췌

실제 작업을 행하는 오브젝트를 감싸서,
실제 오브젝트를 요청하기 전이나 후에 인가 처리(보호)나,
생성 자원이 많이 드는 작업에 대해 백그라운드 처리 (가상), 원격 메소드를 호출하기 위한 작업(원격 프록시) 등을 하는데 사용한다.